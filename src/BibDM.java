import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
public class BibDM{

    /**
     * Ajoute deux entiers
     * @param a le premier entier à ajouter
     * @param b le deuxieme entier à ajouter
     * @return la somme des deux entiers
     */
     public static Integer plus(Integer a, Integer b){
       return a+b;
     }


    /**
     * Renvoie la valeur du plus petit élément d'une liste d'entiers
     * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
     * @param liste
     * @return le plus petit élément de liste
     */
     public static Integer min(List<Integer> liste){
       Integer mini = null;
       for (Integer elem : liste){
         if (mini==null || elem<mini){
           mini = elem;
         }
       }
       return mini;
     }


    /**
     * Teste si tous les élements d'une liste sont plus petits qu'une valeure donnée
     * @param valeur
     * @param liste
     * @return true si tous les elements de liste sont plus grands que valeur.
     */
     public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste){
       for (T elem : liste){
         if (elem.compareTo(valeur)<=0){
           return false;
         }
       }
       return true;
     }


    /**
     * Intersection de deux listes données par ordre croissant.
     * @param liste1 une liste triée
     * @param liste2 une liste triée
     * @return une liste triée avec les éléments communs à liste1 et liste2
     */
     public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2){
       List<T> res = new ArrayList<>();
       if(!liste1.isEmpty() && !liste2.isEmpty()){
         if(liste1.get(liste1.size()-1).compareTo(liste2.get(0))>=0){ //vérifie si le premier élément de la liste2 est plus petit que le dernier élement de la liste1
           for(T elem : liste1){
             if(liste2.contains(elem)){
               if(!res.contains(elem)){
                 res.add(elem);
               }
             }
           }
         }
       }
       return res;
     }


    /**
     * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
     * @param texte une chaine de caractères
     * @return une liste de mots, correspondant aux mots de texte.
     */
     public static List<String> decoupe(String texte){
       List<String> res = new ArrayList<>();
       String mot = "";
       for (int i=0; i<texte.length(); ++i){
         char lettre = texte.charAt(i);
         if(lettre!=' '){
           mot += lettre;
         }
         else{
           if (mot!=""){
             res.add(mot);
             mot = "";
           }
         }
       }
       if (mot!=""){
         res.add(mot);
       }
       return res;
     }


     /**
     * Renvoie le mot le plus présent dans un texte.
     * @param texte une chaine de caractères
     * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
     */
     public static String motMajoritaire(String texte){
       List<String> listeMot = decoupe(texte);
       List<String> motTraite = new ArrayList<>();
       String res = "";
       int cpt = 0;
       int cptMax = 0;
       if(texte!=""){
         for(String mot : listeMot){
           if(res==""){
             res = mot;
           }
           if(!motTraite.contains(mot)){
             for (String elem : listeMot){
               if(elem.equals(mot)){
                 cpt += 1;
               }
               if(cpt>cptMax){
                 res = mot;
                 cptMax = cpt;
               }
               else if (cpt==cptMax){
                 if(res.compareTo(mot)>0){
                   res = mot;
                 }
               }
               cpt = 0;
               motTraite.add(mot);
             }
           }
         }
       }
       else{
         return null;
       }
       return res;
     }


    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de ( et de )
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
     */
     public static boolean bienParenthesee(String chaine){
       int nbParentheses = 0;
       for(int i=0; i<chaine.length();++i){
         if(chaine.charAt(i)=='('){
           nbParentheses += 1;
         }
         else if(chaine.charAt(i)==')'){
           nbParentheses -= 1;
         }
         if(nbParentheses<0){
           return false;
         }
       }
       if (nbParentheses!=0){
         return false;
       }
       return true;
     }


    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
     */
     public static boolean bienParentheseeCrochets(String chaine){
       int nbParentheses = 0;
       int nbCrochets = 0;
       if (chaine.length()!=0){
         for (int i=0; i<chaine.length(); ++i){
           if (chaine.charAt(i)=='('){
             nbParentheses += 1;
             if (chaine.charAt(i+1)==']'){
               return false;
             }
           }
           if (chaine.charAt(i)==')'){
             nbParentheses -= 1;
           }
           if (chaine.charAt(i)=='['){
             nbCrochets += 1;
             if (chaine.charAt(i+1)==')'){
               return false;
             }
           }
           if (chaine.charAt(i)==']'){
             nbCrochets -= 1;
           }
           if (nbCrochets<0 || nbParentheses<0){
             return false;
           }
         }
         if (nbParentheses!= 0 || nbCrochets!=0){
           return false;
         }
       }
       return true;
     }


    /**
     * Recherche par dichtomie d'un élément dans une liste triée
     * @param liste, une liste triée d'entiers
     * @param valeur un entier
     * @return true si l'entier appartient à la liste.
     */
     public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){
       if(liste.isEmpty()){
         return false;
       }
       else{
         int debut = 0;
         int fin = liste.size()-1;
         int milieu = (debut+fin)/2;
         while (debut<=fin){
           milieu = (debut+fin)/2;
           if(liste.get(milieu)<valeur){
             debut = milieu+1;
           }
           else{
             fin = milieu-1;
           }
           if(liste.get(milieu)==valeur){
             return true;
           }
         }
         return false;
       }
     }
}
